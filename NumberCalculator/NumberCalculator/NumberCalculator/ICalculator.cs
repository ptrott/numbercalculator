﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberCalculator {
    public interface ICalculator {

        /// <summary>
        /// Calculates/Generates the answers 
        /// </summary>
        Dictionary<int, string> GenerateAnswers();
    }
}
