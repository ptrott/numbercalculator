﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberCalculator {

    public class CalculateNumbers : ICalculator {

        /// <summary>
        /// Answers based on Numbers used for calculation.
        /// Holds results and how results have been reached.
        /// </summary>
        public Dictionary<int, string> CalculatedAnswers { get; private set; }

        /// <summary>
        /// Numbers to be used to calculate answers.
        /// </summary>
        public List<int> NumbersToBeCalculated { get; private set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="numbers">List of integers to be used for calculator</param>
        public CalculateNumbers(List<int> numbers) {
            if (numbers.Count <= 2) { throw new ArgumentException("numbers (list) argument can not have two or less numbers to calculate answers.", "numbers"); }

            this.NumbersToBeCalculated = numbers;
            this.CalculatedAnswers = new Dictionary<int, string>() { };
        }

        /// <summary>
        /// Calculates answers to the provided list of integers.
        /// Calculates addition of every possible combination of numbers.
        /// </summary>
        /// <returns>Unique answers of addition of all possible number combinations</returns>
        public Dictionary<int, string> GenerateAnswers() {

            List<int> nums = this.NumbersToBeCalculated;
            List<string> binaryResults = new List<string>() { };
            List<int> answers = new List<int>() { };
            List<List<int>> numbersToAdd = new List<List<int>>() { };

            // Calculate binary numbers
            int len = nums.Count;
            int calcs = (int)Math.Pow(2, len) - 1;
            for (int i = 1; i <= calcs; ++i) {
                var binaryNum = Convert.ToString(i, 2).PadLeft(len, '0');
                binaryResults.Add(binaryNum);
            }

            // Find numbers to calculate addition on based on binary numbers
            var index = 0;
            foreach (string binarynum in binaryResults) {
                List<int> numsToAdd = new List<int>() { };

                foreach (char num in binarynum) {

                    if (Convert.ToBoolean(Convert.ToInt32(num.ToString()))) {
                        numsToAdd.Add(nums[index]);
                    }

                    ++index;
                }

                numbersToAdd.Add(numsToAdd);

                index = 0;
            }

            // Add answers to dictionary
            string textOfAnswer = "";
            foreach (List<int> ans in numbersToAdd) {

                foreach (int i in ans) {
                    textOfAnswer += i.ToString() + "+";
                }

                textOfAnswer = textOfAnswer.EndsWith("+") ? textOfAnswer.TrimEnd('+') : textOfAnswer;

                var sum = 0;

                foreach (int num in ans) {
                    sum += num;
                }

                if (!this.CalculatedAnswers.ContainsKey(sum)) {
                    this.CalculatedAnswers.Add(sum,textOfAnswer);
                }

                textOfAnswer = "";
            }

            // Sort answers by key
            IOrderedEnumerable<KeyValuePair<int, string>> generatedAnswers = this.CalculatedAnswers.OrderBy(i => i.Key);
            this.CalculatedAnswers = new Dictionary<int, string>();
            foreach (KeyValuePair<int, string> pair in generatedAnswers) {
                this.CalculatedAnswers.Add(pair.Key, pair.Value);
            }

            return this.CalculatedAnswers;
        }
    }
}
