﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberCalculator;

namespace NumberCalculator.Tests {
    /// <summary>
    /// Summary description for NumberCalculatorTests
    /// </summary>
    [TestClass]
    public class NumberCalculatorTests {

        [TestMethod]
        public void TestNumberCalculator_Constructor_DefaultValues() {

            List<int> nums = new List<int>();
            nums.Add(1);
            nums.Add(2);
            nums.Add(3);

            CalculateNumbers calc = new CalculateNumbers(nums);

            Assert.IsNotNull(calc.CalculatedAnswers);
            CollectionAssert.AreEqual(nums, calc.NumbersToBeCalculated);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "numbers (list) argument can not have two or less numbers to calculate answers.")]
        public void TestNumberCalculator_Constructor_Exception_EmptyList() {

            List<int> nums = new List<int>();
            CalculateNumbers calc = new CalculateNumbers(nums);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "numbers (list) argument can not have two or less numbers to calculate answers.")]
        public void TestNumberCalculator_Constructor_Exception_OneItemList() {

            List<int> nums = new List<int>();
            nums.Add(1);
            CalculateNumbers calc = new CalculateNumbers(nums);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "numbers (list) argument can not have two or less numbers to calculate answers.")]
        public void TestNumberCalculator_Constructor_Exception_TwoItemList() {

            List<int> nums = new List<int>();
            nums.Add(1);
            nums.Add(2);
            CalculateNumbers calc = new CalculateNumbers(nums);
        }

        [TestMethod]
        public void TestNumberCalculator_GenerateAnswers_ThreeNumbers() {
            List<int> nums = new List<int>();
            nums.Add(1);
            nums.Add(2);
            nums.Add(3);

            CalculateNumbers calc = new CalculateNumbers(nums);

            Dictionary<int, string> expected = new Dictionary<int, string>() {
                {1, "1"},
                {2, "2"},
                {3, "3"},
                {4, "1+3"},
                {5, "2+3"},
                {6, "1+2+3"}
            };
            Dictionary<int, string> actual = calc.GenerateAnswers();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNumberCalculator_GenerateAnswers_SevenNumbers() {
            List<int> nums = new List<int>();
            nums.Add(1);
            nums.Add(2);
            nums.Add(3);
            nums.Add(4);
            nums.Add(5);
            nums.Add(6);
            nums.Add(7);

            CalculateNumbers calc = new CalculateNumbers(nums);

            Dictionary<int, string> expected = new Dictionary<int, string>() {
                {1, "1"}, {2, "2"}, {3, "3"}, {4, "4"}, {5, "5"}, {6, "6"}, {7, "7"},
                {8, "3+5"}, {9, "4+5"}, {10, "4+6"}, {11, "5+6"}, {12, "5+7"}, {13, "6+7"},
                {14, "3+5+6"}, {15, "4+5+6"}, {16, "4+5+7"}, {17, "4+6+7"}, {18, "5+6+7"},
                {19, "3+4+5+7"}, {20, "3+4+6+7"}, {21, "3+5+6+7"}, {22, "4+5+6+7"}, 
                {23, "2+3+5+6+7"}, {24, "2+4+5+6+7"}, {25, "3+4+5+6+7"}, {26, "1+3+4+5+6+7"}, 
                {27, "2+3+4+5+6+7"}, {28, "1+2+3+4+5+6+7"}
            };
            Dictionary<int, string> actual = calc.GenerateAnswers();

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
